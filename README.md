# wget

retrieves files from the web

* https://pkgs.alpinelinux.org/packages?name=wget

## License
* https://www.gnu.org/software/wget/

## See also
* https://tracker.debian.org/pkg/wget

## Fossilization
* https://www.drupal.org/node/1369242
* (fr) https://wiki.koumbit.net/Fossilisation
* https://www.google.com/search?q=using+wget+to+mirror+a+website
* [*Download an Entire Website Using Wget in Linux*](https://www.webtipblog.com/index-htm/)
* [*Using wget to mirror a website and everything from the first level of external sites*](https://superuser.com/questions/257794/using-wget-to-mirror-a-website-and-everything-from-the-first-level-of-external-s)
